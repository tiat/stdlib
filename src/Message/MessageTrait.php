<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Message;

//
use Laminas\Diactoros\HeaderSecurity;
use Laminas\Diactoros\Stream;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;
use Tiat\Standard\Request\RequestInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;

use function array_map;
use function array_merge;
use function array_values;
use function implode;
use function is_array;
use function is_resource;
use function is_string;
use function preg_match;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait MessageTrait {
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_headers = [];
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_headerNames = [];
	
	/**
	 * @var StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected StreamInterface $_stream;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_protocol = '1.1';
	
	/**
	 * Retrieves the HTTP protocol version as a string.
	 * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
	 *
	 * @return string HTTP protocol version.
	 * @since   3.0.0 First time introduced.
	 */
	public function getProtocolVersion() : string {
		return $this->_protocol;
	}
	
	/**
	 * Return an instance with the specified HTTP protocol version.
	 * The version string MUST contain only the HTTP version number (e.g.,
	 * "1.1", "1.0").
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * new protocol version.
	 *
	 * @param    string    $version    HTTP protocol version
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withProtocolVersion(string $version) : RequestInterface {
		//
		$this->_validateProtocolVersion($version);
		
		//
		$new             = clone $this;
		$this->_protocol = $version;
		
		//
		return $new;
	}
	
	/**
	 * Validate the HTTP protocol version
	 *
	 * @param    string    $version
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _validateProtocolVersion(string $version) : void {
		if(! empty($version)):
			if(! preg_match('#^(1\.[01]|2(\.0)?)$#', $version)):
				$msg = sprintf('Unsupported HTTP protocol version (%s) provided.', $version);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			throw new InvalidArgumentException('HTTP version can not be empty.');
		endif;
	}
	
	/**
	 * Retrieves all message header values.
	 * The keys represent the header name as it will be sent over the wire, and
	 * each value is an array of strings associated with the header.
	 *     // Represent the headers as a string
	 *     foreach ($message->getHeaders() as $name => $values) {
	 *         echo $name . ': ' . implode(', ', $values);
	 *     }
	 *     // Emit headers iteratively:
	 *     foreach ($message->getHeaders() as $name => $values) {
	 *         foreach ($values as $value) {
	 *             header(sprintf('%s: %s', $name, $value), false);
	 *         }
	 *     }
	 * While header names are not case-sensitive, getHeaders() will preserve the
	 * exact case in which headers were originally specified.
	 *
	 * @return string[][] Returns an associative array of the message's headers.
	 *     Each key MUST be a header name, and each value MUST be an array of
	 *     strings for that header.
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeaders() : array {
		return $this->_headers;
	}
	
	/**
	 * Filter a set of headers to ensure they are in the correct internal format.
	 * Used by message constructors to allow setting all initial headers at once.
	 *
	 * @param    array    $originals
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setHeaders(array $originals) : void {
		//
		$names = $headers = [];
		
		//
		foreach($originals as $header => $value):
			//
			$value = $this->_filterHeaderValue($value);
			
			//
			$this->_assertHeader($header);
			
			//
			$names[strtolower($header)] = $header;
			$headers[$header]           = $value;
		endforeach;
		
		//
		$this->_headerNames = $names;
		$this->_headers     = $headers;
	}
	
	/**
	 * @param    mixed    $values
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	private function _filterHeaderValue(mixed $values) : array {
		//
		if(empty($values)):
			throw new InvalidArgumentException('Invalid header value: must be a string or array or string.');
		endif;
		
		//
		if(! is_array($values)):
			$values = [$values];
		endif;
		
		//
		return array_map(static function ($value) {
			//
			HeaderSecurity::assertValid($value);
			
			//
			return (string)$value;
		}, array_values($values));
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _assertHeader(string $name) : void {
		HeaderSecurity::assertValidName($name);
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeaderLine(string $name) : string {
		//
		if(! empty($value = $this->getHeader($name))):
			return implode(',', $value);
		else:
			return '';
		endif;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeader(string $name) : array {
		if(! $this->hasHeader($name)):
			return [];
		endif;
		
		//
		$header = $this->_headerNames[strtolower($name)];
		
		//
		return (array)$this->_headers[$header];
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasHeader(string $name) : bool {
		return isset($this->_headerNames[strtolower($name)]);
	}
	
	/**
	 * @param    string    $name
	 * @param              $value
	 *
	 * @return MessageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withAddedHeader(string $name, $value) : MessageInterface {
		//
		$this->_assertHeader($name);
		
		//
		if(! $this->hasHeader($name)):
			return $this->withHeader($name, $value);
		endif;
		
		//
		$header = $this->_headerNames[strtolower($name)];
		
		//
		$new                    = clone $this;
		$value                  = $this->_filterHeaderValue($value);
		$new->_headers[$header] = array_merge($this->_headers[$header], $value);
		
		//
		return $new;
	}
	
	/**
	 * @param    string    $name
	 * @param              $value
	 *
	 * @return MessageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withHeader(string $name, $value) : MessageInterface {
		//
		$this->_assertHeader($name);
		
		//
		$header = strtolower($name);
		
		//
		$new = clone $this;
		if($new->hasHeader($name)):
			unset($new->_headers[$new->_headerNames[$header]]);
		endif;
		
		//
		$value = $this->_filterHeaderValue($value);
		
		//
		$new->_headerNames[$header] = $name;
		$new->_headers[$name]       = $value;
		
		//
		return $new;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withoutHeader(string $name) : RequestInterface {
		//
		if(! $this->hasHeader($name)):
			return clone $this;
		endif;
		
		//
		$header   = strtolower($name);
		$original = $this->_headerNames[$header];
		
		//
		$new = clone $this;
		unset($new->_headers[$original], $new->_headerNames[$header]);
		
		//
		return $new;
	}
	
	/**
	 * @return StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getBody() : StreamInterface {
		return $this->_stream;
	}
	
	/**
	 * @param    StreamInterface    $body
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withBody(StreamInterface $body) : RequestInterface {
		//
		$new          = clone $this;
		$new->_stream = $body;
		
		//
		return $new;
	}
	
	/**
	 * @param    mixed     $stream
	 * @param    string    $mode
	 *
	 * @return StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getStream(mixed $stream, string $mode) : StreamInterface {
		//
		if($stream instanceof StreamInterface):
			return $stream;
		endif;
		
		//
		if(! is_string($stream) && ! is_resource($stream)):
			throw new InvalidArgumentException('Stream must be a string stream resource identifier, an actual stream resource or a Tiat\Psr\Http\StreamInterface implemation.');
		endif;
		
		//
		return new Stream($stream, $mode);
	}
}
