<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Loader;

//
use ReflectionClass;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Stdlib\Exception\BadMethodCallException;
use Tiat\Stdlib\Exception\RuntimeException;
use Tiat\Stdlib\Register\RegisterContainerInterface;

use function array_walk;
use function explode;
use function get_debug_type;
use function implode;
use function method_exists;
use function sprintf;
use function strtolower;
use function ucfirst;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait ClassLoaderTrait {
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_autoinit = FALSE;
	
	/**
	 * This is like C/C++ type main() function
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_autorun = FALSE;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_initCompleted = FALSE;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_hasRun = FALSE;
	
	/**
	 * @param    callable    $obj
	 * @param    array       $settings
	 *
	 * @return void
	 */
	public function setSettings(callable $obj, array $settings) : void {
		//
		$set = 'set';
		
		//
		foreach($settings as $key => $val):
			// Define setter function name
			$parts = explode('_', $key);
			array_walk($parts, static function (&$val) {
				$val = ucfirst(strtolower($val));
			});
			$name = $set . ( implode('', $parts) );
			
			//
			if(method_exists($obj, $name)):
				if($val instanceof RegisterContainerInterface):
					$obj->$name($val->getParams());
				else:
					$obj->$name($val);
				endif;
			else:
				$msg = sprintf("The method %s doesn't exists in class %s.", $name, get_debug_type($obj));
				throw new BadMethodCallException($msg);
			endif;
		endforeach;
	}
	
	/**
	 * @param    bool    $autoinit
	 * @param    bool    $autorun
	 * @param            ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _defineAutostart(bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) : void {
		//
		$this->setAutoInit($autoinit)->setAutoRun($autorun);
		
		//
		if($this->getAutoInit() === TRUE):
			$this->init(...$args);
		endif;
		
		//
		if($this->getAutorun() === TRUE):
			//
			$this->run(...$args);
			
			//
			if(method_exists($this, 'setOutputCode')):
				$this->setOutputCode(200);
			endif;
		endif;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getAutoinit() : bool {
		return $this->_autoinit;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setAutoinit(bool $status) : ClassLoaderInterface {
		//
		$this->_autoinit = $status;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : ClassLoaderInterface {
		//
		if($this->isInitCompleted() === FALSE):
			$this->setInitCompleted(TRUE);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isInitCompleted() : bool {
		return $this->_initCompleted;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setInitCompleted(bool $status) : ClassLoaderInterface {
		//
		$this->_initCompleted = $status;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getAutorun() : bool {
		return $this->_autorun;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setAutorun(bool $status) : ClassLoaderInterface {
		//
		$this->_autorun = $status;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ClassLoaderInterface {
		// Check that init is completed
		if($this->checkInit($this->getAutoinit(), ...$args) === TRUE):
			//
			if($this->hasRun() === TRUE):
				$msg = sprintf("This module (%s) has already been run and can't be executed more than once.",
				               ( ( new ReflectionClass($this) )->getParentClass() )->getName());
				throw new RuntimeException($msg);
			endif;
			
			// As default this is always false so variable can be safely empty
			// Application is already executed so merge the flag
			if(isset($args['hasRun']) && $args['hasRun'] === TRUE):
				$this->setRun(TRUE);
			endif;
			
			//
			return $this;
		endif;
		
		//
		$msg = sprintf("%s initialization is not completed.", __CLASS__);
		throw new RuntimeException($msg);
	}
	
	/**
	 * @param    bool    $init
	 * @param            ...$args
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkInit(bool $init, ...$args) : bool {
		//
		if($init === TRUE && $this->isInitCompleted() === FALSE):
			$this->init(...$args);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasRun() : bool {
		//
		if(! empty($this->_hasRun)):
			return $this->_hasRun;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRun(bool $status) : ClassLoaderInterface {
		//
		$this->_hasRun = $status;
		
		//
		return $this;
	}
}
