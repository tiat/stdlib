<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Exception;

//
use RuntimeException;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class UnseekableStreamException extends RuntimeException implements ExceptionInterface {
	
	/**
	 * @return static
	 * @since   3.0.0 First time introduced.
	 */
	public static function dueToConfiguration() : self {
		return new self('Stream is not seekable');
	}
	
	/**
	 * @return static
	 * @since   3.0.0 First time introduced.
	 */
	public static function dueToMissingResource() : self {
		return new self('No resource available; cannot seek position');
	}
	
	/**
	 * @return static
	 * @since   3.0.0 First time introduced.
	 */
	public static function dueToPhpError() : self {
		return new self('Error seeking within stream');
	}
	
	/**
	 * @return static
	 * @since   3.0.0 First time introduced.
	 */
	public static function forCallbackStream() : self {
		return new self('Callback streams cannot seek position');
	}
}
