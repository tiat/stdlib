<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Random;

//
use Exception;
use Random\Randomizer;
use Tiat\Stdlib\Exception\InvalidArgumentException;

use function ctype_alpha;
use function ctype_digit;
use function ctype_punct;
use function implode;
use function is_array;
use function is_string;
use function random_int;
use function range;
use function sprintf;
use function substr;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class RandomGenerator implements RandomGeneratorInterface {
	
	/**
	 * @param    int    $min
	 * @param    int    $max
	 *
	 * @return int
	 * @throws Exception
	 * @since   3.0.0 First time introduced.
	 */
	public function getNumber(int $min = PHP_INT_MIN, int $max = PHP_INT_MAX) : int {
		// Check if min is a positive number
		if($min >= PHP_INT_MIN):
			//
			if($max > PHP_INT_MAX):
				$msg = sprintf("Max value (%u) is bigger than allowed (%u).", $max, PHP_INT_MAX);
				throw new InvalidArgumentException($msg);
			endif;
			
			//
			return random_int($min, $max);
		else:
			$msg = sprintf("Min value (%u) is smaller than allowed (%u).", $max, PHP_INT_MIN);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    int                  $length
	 * @param    bool|string|array    $letters
	 * @param    bool|string|array    $numbers
	 * @param    bool|string|array    $specials
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getString(int $length = 8, bool|string|array $letters = TRUE, bool|string|array $numbers = TRUE, bool|string|array $specials = TRUE) : string {
		// Get letters
		if($letters === TRUE || ! empty($letters)):
			$a = $this->_getStringLetters($letters);
		endif;
		
		// Get numbers
		if($numbers === TRUE || ! empty($numbers)):
			$b = $this->_getStringNumbers($numbers);
		endif;
		
		// Get specials
		if($specials === TRUE || ! empty($specials)):
			$d = $this->_getStringSpecials($specials);
		endif;
		
		// Concatenate letters, numbers and specials
		$string = ( $a ?? '' ) . ( $b ?? '' ) . ( $d ?? '' );
		
		// Shuffle the string
		return substr(( new Randomizer() )->shuffleBytes($string), 0, $length);
	}
	
	/**
	 * @param    bool|string|array    $letters
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getStringLetters(bool|string|array $letters) : string {
		// Check if letters are a string of alphabetic characters or an array of alphabetic characters
		if(is_string($letters) && ctype_alpha($letters) === TRUE):
			return $letters;
		elseif(is_array($letters) && ( $return = implode('', $letters) ) && ctype_alpha($return)):
			return $return;
		elseif($letters === TRUE):
			// Generate a string of lowercase and uppercase alphabetic characters
			$a = range('a', 'z');
			$b = range('A', 'Z');
			
			// Combine the ranges and return as a string
			return implode('', [...$a, ...$b]);
		else:
			$msg = sprintf("Given value (%s) is not supported by '%s'.", $letters, __METHOD__);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    bool|string|array    $numbers
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getStringNumbers(bool|string|array $numbers) : string {
		// Check if numbers are a string of numeric characters or an array of numeric characters
		if(is_string($numbers) && ctype_digit($numbers)):
			return $numbers;
		elseif(is_array($numbers) && ( $return = implode('', $numbers) ) && ctype_digit($return)):
			return $return;
		elseif($numbers === TRUE):
			return implode('', range(0, 9));
		else:
			$msg = sprintf("Given value (%s) is not supported by '%s'.", $numbers, __METHOD__);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    bool|string|array    $specials
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getStringSpecials(bool|string|array $specials) : string {
		// Check if specials are a string of punctuation characters or an array of punctuation characters
		if(is_string($specials) && ctype_punct($specials)):
			return $specials;
		elseif(is_array($specials) && ( $return = implode('', $specials) ) && ctype_punct($return)):
			return $return;
		elseif($specials === TRUE):
			return self::STRING_SPECIALS;
		else:
			$msg = sprintf("Given value (%s) is not supported by '%s'.", $specials, __METHOD__);
			throw new InvalidArgumentException($msg);
		endif;
	}
}
