<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Random;

/**
 * Interface RandomGeneratorInterface
 * The RandomGeneratorInterface defines a contract for generating random values.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RandomGeneratorInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string STRING_SPECIALS = "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
	
	/**
	 * Generates a random integer within the specified range.
	 *
	 * @param    int    $min
	 * @param    int    $max
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getNumber(int $min = PHP_INT_MIN, int $max = PHP_INT_MAX) : int;
	
	/**
	 * Generates a random string based on the specified criteria.
	 *
	 * @param    int                  $length      The length of the random string to generate. Default is 8.
	 * @param    bool|string|array    $letters     Characters or criteria for letters to include. TRUE for default letters. Default is TRUE.
	 * @param    bool|string|array    $numbers     Characters or criteria for numbers to include. TRUE for default numbers. Default is TRUE.
	 * @param    bool|string|array    $specials    Characters or criteria for special characters to include. TRUE for default special characters. Default is TRUE.
	 *
	 * @return string The generated random string.
	 * @since   3.0.0 First time introduced.
	 */
	public function getString(int $length = 8, bool|string|array $letters = TRUE, bool|string|array $numbers = TRUE, bool|string|array $specials = TRUE) : string;
}