<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

//
use function is_object;
use function trigger_error;

/**
 * Class Client
 * Extends the DOMDocument class to provide methods for converting XML to arrays and vice versa.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class XmlClient extends AbstractXmlClient {
	
	/**
	 * Convert an XML to Array
	 *
	 * @param    string    $input
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	final public function createArray(string $input) : ?array {
		// Load the XML into the DOMDocument object
		if(@$this->loadXML($input, LIBXML_BIGLINES | LIBXML_PARSEHUGE) === FALSE):
			trigger_error("Given XML is not valid.", E_USER_NOTICE);
		endif;
		
		// Convert XML to PHP array
		if(is_object($this->documentElement)):
			// Create an array to hold the XML data
			$array[$this->documentElement->tagName] = $this->convert($this->documentElement);
			
			//
			return $array;
		endif;
		
		//
		return NULL;
	}
	
}
