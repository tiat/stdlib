<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

/**
 * XmlClientInterface defines the methods required to interact with XML-based services.
 * It should be implemented by any class that needs to communicate with such services.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface XmlClientInterface {
	
	/**
	 * Creates an array from the given input string.
	 *
	 * @param    string    $input    The input string that needs to be converted to an array.
	 *
	 * @return array|null The array representation of the input string or null if conversion fails.
	 * @since   3.1.0 First time introduced.
	 */
	public function createArray(string $input) : ?array;
}