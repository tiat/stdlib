<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

/**
 * Server class for converting an array to an XML structure.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class XmlServer extends AbstractXmlServer {
	
	/**
	 * Convert an Array to XML
	 *
	 * @param    string    $nodeName
	 * @param    array     $arr    Array to be converted
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function createXML(string $nodeName, array $arr = []) : static {
		//
		$this->appendChild($this->convert($nodeName, $arr));
		
		//
		return $this;
	}
	
}
