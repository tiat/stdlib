<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

/**
 * Abstract class intended for converting XML data to various formats
 * This class provides a foundation for implementing XML conversion functionalities.
 * Subclasses should extend this class and implement specific conversion methods.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractXmlConvert implements XmlConvertInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_version = '1.0';
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_encoding = 'UTF-8';
	
	/**
	 * @var XmlServerInterface
	 * @since   3.1.0 First time introduced.
	 */
	private XmlServerInterface $_dom;
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getVersion() : string {
		return $this->_version;
	}
	
	/**
	 * @param    string    $version
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setVersion(string $version) : static {
		//
		$this->_version = $version;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getEncoding() : string {
		return $this->_encoding;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : static {
		//
		$this->_encoding = strtoupper($encoding);
		
		//
		return $this;
	}
	
	/**
	 * @return null|XmlServerInterface
	 * @since   3.1.0 First time introduced.
	 */
	protected function _getDom() : ?XmlServerInterface {
		return $this->_dom ?? NULL;
	}
	
	/**
	 * @param    XmlServerInterface    $dom
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	protected function _setDom(XmlServerInterface $dom) : void {
		$this->_dom = $dom;
	}
}