<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

/**
 * Interface XmlConvertInterface
 * Defines methods for converting objects to and from XML.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface XmlConvertInterface {
	
	/**
	 * Get XML version.
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getVersion() : string;
	
	/**
	 * Set XML version.
	 *
	 * @param    string    $version
	 *
	 * @return XmlConvertInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setVersion(string $version) : XmlConvertInterface;
	
	/**
	 * Get XML encoding.
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getEncoding() : string;
	
	/**
	 * Set XML encoding.
	 *
	 * @param    string    $encoding
	 *
	 * @return XmlConvertInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : XmlConvertInterface;
	
	/**
	 * Return XML as PHP array. If content is not valid XML or there is no content then NULL will be returned.
	 *
	 * @param    string    $xml
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function toArray(string $xml) : ?array;
	
	/**
	 * Convert PHP array to XML. Returns true if conversion was successful, false otherwise.
	 *
	 * @param    string|array    $content
	 * @param    string          $name
	 *
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function toXml(string|array $content, string $name = 'root') : bool;
	
	/**
	 * Get XML as a file. Returns the number of bytes written or false if an error occurred.
	 *
	 * @param    string    $filename
	 *
	 * @return null|int|false
	 * @since   3.1.0 First time introduced.
	 */
	public function getXmlAsFile(string $filename) : int|false|null;
	
	/**
	 * Retrieve an XML string representation.
	 * Might return false on failure, null if no data, or the XML string if successful.
	 *
	 * @return false|string|null
	 * @since   3.1.0 First time introduced.
	 */
	public function getXmlAsString() : false|string|null;
}