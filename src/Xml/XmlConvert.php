<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

//
use Tiat\Stdlib\Exception\InvalidArgumentException;

use function sprintf;

/**
 * Class Convert
 * Provides methods to convert XML to array and vice versa. Also supports
 * setting version and encoding for the conversion process and offers methods
 * to retrieve the XML in string or file format.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class XmlConvert extends AbstractXmlConvert {
	
	/**
	 * @param    string    $xml
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function toArray(string $xml) : ?array {
		//
		$dom = new XmlClient();
		
		// Return array as it is
		if(( $tmp = $dom->createArray($xml) ) !== NULL):
			return $tmp;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string|array    $content
	 * @param    string          $name
	 *
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function toXml(string|array $content, string $name = 'root') : bool {
		// Check if content is an array
		if(! empty($name)):
			//
			$this->_setDom(new XmlServer($this->getVersion(), $this->getEncoding()));
			
			//
			if(( $dom = $this->_getDom() ) !== NULL):
				//
				$dom->createXML($name, $content);
				
				//
				return TRUE;
			endif;
		else:
			$msg = sprintf("Name must have an value, got '%s'.", $name);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * Dumps the internal XML tree back into a file. Returns the number of bytes written or false if an error occurred.
	 * If there are no DOM tree then NULL will be returned.
	 *
	 * @param    string    $filename
	 *
	 * @return int|false|null
	 * @since   3.1.0 First time introduced.
	 */
	public function getXmlAsFile(string $filename) : int|false|null {
		// Check if DOM tree exists
		if(( $dom = $this->_getDom() ) !== NULL):
			return $dom->save($filename);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Get XML as a string
	 *
	 * @return null|false|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getXmlAsString() : false|string|null {
		// Check if DOM tree exists
		if(( $dom = $this->_getDom() ) !== NULL):
			//
			$dom->preserveWhiteSpace = FALSE;
			$dom->formatOutput       = TRUE;
			
			//
			return $dom->saveXML();
		endif;
		
		//
		return NULL;
	}
}
