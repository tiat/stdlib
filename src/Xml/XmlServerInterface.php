<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

/**
 * This interface defines the contract for XML server interactions.
 * Classes implementing this interface should handle specific
 * XML-based server communication and interactions.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface XmlServerInterface {
	
	/**
	 * Creates an XML representation of the provided node and array.
	 *
	 * @param    string    $nodeName    The name of the root node for the XML document.
	 * @param    array     $arr         An associative array to be converted into XML elements under the root node.
	 *
	 * @return XmlServerInterface Returns an instance of XmlServerInterface containing the generated XML.
	 * @since   3.1.0 First time introduced.
	 */
	public function createXML(string $nodeName, array $arr) : XmlServerInterface;
}