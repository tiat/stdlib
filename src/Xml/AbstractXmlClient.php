<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

//
use DOMDocument;
use DOMElement;
use DOMNode;

use function count;
use function is_array;
use function trim;

/**
 * AbstractXmlClient class serves as a base class for XML clients by extending
 * the functionality of the DOMDocument. This class is intended to be extended
 * by other classes which require XML processing capabilities.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractXmlClient extends DOMDocument implements XmlClientInterface {
	
	/**
	 * Converts a provided DOMElement, DOMNode, or string into an array or string.
	 *
	 * @param    DOMElement|DOMNode|string    $node    The input node or string to be converted.
	 *
	 * @return array|string The converted representation of the input node or string.
	 * @since   3.1.0 First time introduced.
	 */
	protected function convert(DOMElement|DOMNode|string $node) : array|string {
		//
		$output = [];
		
		// If the node is a text node, CDATA section, or element, convert it to an associative array
		switch($node->nodeType):
			// Case CDATA section
			case XML_CDATA_SECTION_NODE:
				$output['@cdata'] = trim($node->textContent);
			break;
			
			// Case text node
			case XML_TEXT_NODE:
				$output = trim($node->textContent);
			break;
			
			// Case element node
			case XML_ELEMENT_NODE:
				// For each child node, call the covert function recursively
				for($i = 0, $m = $node->childNodes->length; $i < $m; $i++):
					// Get the child node
					$child = $node->childNodes->item($i);
					$v     = $this->convert($child);
					
					// If the child node has a tag name, create an associative array with the tag name as the key
					if(isset($child->tagName)):
						$t = $child?->tagName;
						
						// Assume more nodes of same kind are coming
						if(! isset($output[$t])):
							$output[$t] = [];
						endif;
						
						// If it is an array, append the value
						if(is_array($output)):
							$output[$t][] = $v;
						endif;
					elseif(! empty($v)):
						// Check if it is not an empty text node
						$output = $v;
					endif;
				endfor;
				
				// Check if the output is an array, if not, convert it to a string
				if(is_array($output)):
					// If only one node of its kind, assign it directly instead if array($value);
					foreach($output as $t => $v):
						if(is_array($v) && count($v) === 1):
							$output[$t] = $v[0];
						endif;
					endforeach;
					
					// For empty nodes
					if(empty($output)):
						$output = '';
					endif;
				endif;
				
				// Loop through the attributes and collect them
				if($node->attributes->length):
					// Create an associative array to hold the attributes
					$a = [];
					
					// Loop through the attributes and collect them into an associative array
					foreach($node->attributes as $attrName => $attrNode):
						$a[(int)$attrName] = (string)$attrNode->value;
					endforeach;
					
					// If it's a leaf node, store the value in @value instead of directly storing it.
					if(! is_array($output)):
						$output = ['@value' => $output];
					endif;
					
					// Store the attributes in the output array
					$output['@attributes'] = $a;
				endif;
			break;
		endswitch;
		
		// Return the converted array or string
		return $output;
	}
}