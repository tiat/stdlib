<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Xml;

//
use DOMDocument;
use DOMException;
use DOMNode;
use Tiat\Stdlib\Exception\DomainException;
use Tiat\Stdlib\Exception\RuntimeException;

use function is_array;
use function is_numeric;
use function key;
use function preg_match;
use function sprintf;
use function trim;

/**
 * AbstractXmlServer is a base class for XML-based server implementations.
 * It defines the core methods and properties that any XML server should have.
 * Derived classes should provide specific implementations for handling XML data.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractXmlServer extends DOMDocument implements XmlServerInterface {
	
	/**
	 * Convert an Array to XML
	 *
	 * @param    string               $nodeName    Name of the root node to be converted
	 * @param    null|array|string    $arr         Array|string to be converted
	 *
	 * @return DOMNode
	 * @since   3.1.0 First time introduced.
	 */
	protected function &convert(string $nodeName, array|string|null $arr = NULL) : DOMNode {
		//
		try {
			if(trim($nodeName) !== ''):
				//
				$node = $this->createElement($nodeName);
				
				//
				if(is_array($arr)) {
					// Get the attributes first.;
					if(isset($arr['@attributes'])):
						foreach($arr['@attributes'] as $key => $value):
							if(! $this->isValidTagName($key)):
								$msg = sprintf("Tagname '%s' is not valid", $key);
								throw new RuntimeException($msg);
							endif;
							
							//
							$node->setAttribute($key, $this->bool2str($value));
						endforeach;
						
						// Remove the key from the array once done.
						unset($arr['@attributes']);
					endif;
					
					// Check if it has a value stored in @value, if yes store the value and return
					// Else check if it's directly stored as string
					if(isset($arr['@value'])):
						$node->appendChild($this->createTextNode($this->bool2str($arr['@value'])));
						
						// Remove the key from the array once done
						unset($arr['@value']);
						
						// Return from recursion, as a note with value cannot have child nodes.
						return $node;
					elseif(isset($arr['@cdata'])):
						$node->appendChild($this->createCDATASection($this->bool2str($arr['@cdata'])));
						
						// Remove the key from the array once done.
						unset($arr['@cdata']);
						
						// Return from recursion, as a note with cdata cannot have child nodes.
						return $node;
					endif;
				}
				
				// Create sub node using recursion
				if(is_array($arr)):
					// Recurse to get the node for that key
					foreach($arr as $key => $value):
						//
						if(! $this->isValidTagName($key)):
							$msg = sprintf("[Array2XML] Illegal character in tag name. Tag: '%s' in node: %s", $key,
							               $nodeName);
							throw new RuntimeException($msg);
						endif;
						
						//
						if(is_array($value) && is_numeric(key($value))):
							// MORE THAN ONE NODE OF ITS KIND;
							// If the new array is numeric index, means it's the array of nodes of the same kind
							// It should follow the parent key name
							foreach($value as $v):
								$node->appendChild($this->convert($key, $v));
							endforeach;
						else:
							// ONLY ONE NODE OF ITS KIND
							$node->appendChild($this->convert($key, $value));
						endif;
						
						// Remove the key from the array once done.
						unset($arr[$key]);
					endforeach;
				endif;
				
				// After we are done with all the keys in the array (if it is one)
				// We check if it has any text value, if yes, append it.
				if(! is_array($arr)):
					$node->appendChild($this->createTextNode($this->bool2str($arr)));
				endif;
				
				//
				return $node;
			endif;
		} catch(DOMException $e) {
			throw new DomainException($e->getMessage());
		}
		
		//
		return $this;
	}
	
	/**
	 * Get string representation of boolean value
	 *
	 * @param $tag
	 *
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	protected function isValidTagName($tag) : bool {
		return ( preg_match('/^[a-z_]+[a-z0-9:\-._]*[^:]*$/i', $tag, $matches) && $matches[0] === $tag );
	}
	
	/**
	 * Check if the tag name or attribute name contains illegal characters
	 *
	 * @see     http://www.w3.org/TR/xml/#sec-common-syn
	 *
	 * @param    bool|string    $v
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	protected function bool2str(bool|string $v) : string {
		// If the value is already a string, return it as is
		$v = $v === TRUE ? 'true' : $v;
		
		// If the value is a boolean, convert it to a string
		return $v === FALSE ? 'false' : $v;
	}
}