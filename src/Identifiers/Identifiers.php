<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Identifiers;

//
use DateTimeInterface;
use Ramsey\Uuid\Fields\FieldsInterface;
use Ramsey\Uuid\Provider\Node\StaticNodeProvider;
use Ramsey\Uuid\Type\Hexadecimal;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tiat\Standard\Identifiers\IdentifiersInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Tiat\Stdlib\Exception\RuntimeException;
use Tuupola\Base32;

use function is_string;
use function sprintf;
use function str_pad;
use function strlen;
use function strtolower;
use function substr;

/**
 * Class Identifiers generates UUIDs and ulids using the Crockford Base32 encoding.
 *
 * @version 3.2.0
 * @since   3.2.0 First time introduced.
 */
class Identifiers implements IdentifiersInterface {
	
	/**
	 * @var Base32
	 * @since   3.2.0 First time introduced.
	 */
	private Base32 $_crockfordSettings;
	
	/**
	 * @var UuidInterface
	 * @since   3.2.0 First time introduced.
	 */
	private UuidInterface $_uuid;
	
	/**
	 * @var int
	 * @since   3.2.0 First time introduced.
	 */
	private int $_uuidVersion;
	
	/**
	 * @param    string    $binary
	 *
	 * @return string
	 * @since   3.2.0 First time introduced.
	 */
	public function binaryToUuid(string $binary) : string {
		return Uuid::fromBytes($binary)->toString();
	}
	
	/**
	 * @param    NULL|UuidInterface    $uuid
	 *
	 * @return string
	 * @since   3.2.0 First time introduced.
	 */
	public function convertUuidToBinary(?UUidInterface $uuid = NULL) : string {
		//
		if($uuid === NULL):
			$uuid = $this->getUuid();
		endif;
		
		// Convert UUID to binary
		return $uuid->getBytes();
	}
	
	/**
	 * @return null|UuidInterface
	 * @since   3.2.0 First time introduced.
	 */
	public function getUuid() : ?UuidInterface {
		return $this->_uuid ?? NULL;
	}
	
	/**
	 * @param    UuidInterface    $uuid
	 *
	 * @return void
	 * @since   3.2.0 First time introduced.
	 */
	public function setUuid(UuidInterface $uuid) : void {
		if(( $f = $uuid->getFields() ) instanceof FieldsInterface):
			$this->_uuidVersion = $f->getVersion();
		endif;
		
		//
		$this->_uuid = $uuid;
	}
	
	/**
	 * @param    string    $binary
	 *
	 * @return string
	 * @since   3.2.0 First time introduced.
	 */
	public function binaryToUlid(string $binary) : string {
		// Convert binary to ULID
		if(( $base32 = $this->getCrockfordSettings() ) !== NULL) {
			// Ensure the binary is 16 bytes long
			if(strlen($binary) !== 16):
				$msg = sprintf('Invalid binary length. Expected 16 bytes, got %d', strlen($binary));
				throw new InvalidArgumentException($msg);
			endif;
			
			// Pad the binary data to 20 bytes
			$paddedBinary = "\x00\x00\x00\x00" . $binary;
			
			// Encode the padded binary data to Crockford Base32
			$encoded = $base32->encode($paddedBinary);
			
			// Remove the first 6 characters (hyphens)
			return substr($encoded, 6); // Use the last 26 characters
		}
		
		$msg = sprintf('Crockford Base32 encoding is not available. Please install the %s package.', Base32::class);
		throw new InvalidArgumentException($msg);
	}
	
	/**
	 * @return null|Base32
	 * @since   3.2.0 First time introduced.
	 */
	public function getCrockfordSettings() : ?Base32 {
		// If not set, create the Crockford settings
		if(empty($this->_crockfordSettings)):
			$this->setCrockfordSettings();
		endif;
		
		//
		return $this->_crockfordSettings ?? NULL;
	}
	
	/**
	 * @param    null|Base32    $settings    The Base32 settings object.
	 *
	 * @return void
	 * @since   3.2.0 First time introduced.
	 */
	public function setCrockfordSettings(?Base32 $settings = NULL) : void {
		if($settings === NULL):
			$this->_crockfordSettings =
				new Base32(['characters' => Base32::CROCKFORD, 'padding' => FALSE, 'crockford' => TRUE,]);
		else:
			$this->_crockfordSettings = $settings;
		endif;
	}
	
	/**
	 * @param    string    $ulid
	 *
	 * @return string
	 * @since   3.2.0 First time introduced.
	 */
	public function convertUlidToBinary(string $ulid) : string {
		// If not set, create the Crockford settings
		if(( $crockford = $this->getCrockfordSettings() ) !== NULL) {
			// Check if the ULID length is valid
			if(strlen($ulid) !== 26):
				$msg = sprintf('Invalid ULID length. Expected 26 characters, got %d', strlen($ulid));
				throw new InvalidArgumentException($msg);
			endif;
			
			// Pad the ULID with zeros to make it a full 32-character Base32 string
			$paddedUlid = '000000' . $ulid; // Add 6 zeros to the front to make it a full 32-character Base32 string
			$binary     = $crockford->decode($paddedUlid);
			
			//
			if(strlen($binary) !== 20):
				$msg = sprintf('Invalid decoded binary length. Expected 20 bytes, got %d', strlen($binary));
				throw new InvalidArgumentException($msg);
			endif;
			
			//
			return substr($binary, 4);
		}
		
		// If Crockford Base32 encoding is not available, throw an exception
		$msg = sprintf('Crockford Base32 encoding is not available. Please install the %s package.', Base32::class);
		throw new InvalidArgumentException($msg);
	}
	
	/**
	 * @param    null|UuidInterface    $uuid
	 *
	 * @return string
	 * @since   3.2.0 First time introduced.
	 */
	public function createUlid(?UuidInterface $uuid = NULL) : string {
		// Check if UUID version is 7
		if($uuid === NULL):
			$this->setUuid($this->getUuid() ?? $this->createUuidV7());
		else:
			$this->setUuid($uuid);
		endif;
		
		// Check if UUID version is 7
		if($this->getUuid() !== NULL && $this->_checkUuidVersionForUlid($this->getUuid()) === TRUE):
			// If Crockford Base32 encoding is available, create the ULID
			if(( $crockford = $this->getCrockfordSettings() ) !== NULL):
				// Convert UUID to binary and create the ULID from it
				$bytes   = str_pad($this->getUuid()->getBytes(), 20, "\x00", STR_PAD_LEFT);
				$encoded = substr($crockford->encode($bytes), 6); // Use the last 26 characters
				
				// Ensure the encoded string is 26 characters long
				if(strlen($encoded) !== 26):
					$msg = sprintf('Invalid ULID length. Expected 26 characters, got %d', strlen($encoded));
					throw new InvalidArgumentException($msg);
				endif;
				
				// Return the ULID as a string
				return $encoded;
			endif;
			
			//
			$msg = sprintf('Crockford Base32 encoding is not available. Please install the %s package.', Base32::class);
			throw new RuntimeException($msg);
		endif;
		
		// If UUID version is not 7, throw an exception
		$msg = sprintf("Can't create ULID from UUID version %u.", $this->getUuidVersion());
		throw new InvalidArgumentException($msg);
	}
	
	/**
	 * @param    null|DateTimeInterface    $datetime
	 *
	 * @return UuidInterface
	 * @since   3.2.0 First time introduced.
	 */
	public function createUuidV7(?DateTimeInterface $datetime = NULL) : UuidInterface {
		//
		$this->_uuidVersion = 7;
		
		//
		return Uuid::uuid7($datetime ?? NULL);
	}
	
	/**
	 * @param    UuidInterface    $uuid
	 *
	 * @return bool
	 * @since   3.2.0 First time introduced.
	 */
	protected function _checkUuidVersionForUlid(UuidInterface $uuid) : bool {
		if(( $f = $uuid->getFields() ) instanceof FieldsInterface):
			// Save UUID interface & version for later use.
			$this->setUuid($uuid);
			
			//
			return $f->getVersion() === 7;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return null|int
	 * @since   3.2.0 First time introduced.
	 */
	public function getUuidVersion() : ?int {
		return $this->_uuidVersion ?? NULL;
	}
	
	/**
	 * @param    int      $version
	 * @param    mixed    ...$args
	 *
	 * @return string
	 * @since   3.2.0 First time introduced.
	 */
	public function createUuid(int $version, ...$args) : string {
		$this->_uuid = match ( $version ) {
			1 => $this->createUuidV1(...$args),
			4 => $this->createUuidV4(),
			5 => $this->createUuidV5(...$args),
			7 => $this->createUuidV7(...$args),
			default => throw new InvalidArgumentException('Unsupported UUID version'),
		};
		
		//
		return $this->_uuid->toString();
	}
	
	/**
	 * @param    null|string    $macAddress
	 * @param    null|int       $clockSequence
	 *
	 * @return UuidInterface
	 * @since   3.2.0 First time introduced.
	 */
	public function createUuidV1(?string $macAddress = NULL, ?int $clockSequence = NULL) : UuidInterface {
		// Create a random node provider if not provided.
		if(! empty($macAddress)):
			$node = new StaticNodeProvider(new Hexadecimal($macAddress));
		endif;
		
		//
		$this->_uuidVersion = 1;
		
		//
		return Uuid::uuid1($node ?? NULL, $clockSequence ?? NULL);
	}
	
	/**
	 * @return UuidInterface
	 * @since   3.2.0 First time introduced.
	 */
	public function createUuidV4() : UuidInterface {
		//
		$this->_uuidVersion = 4;
		
		//
		return Uuid::uuid4();
	}
	
	/**
	 * @param    UuidInterface|string    $namespace
	 * @param    string                  $name
	 *
	 * @return UuidInterface
	 * @since   3.2.0 First time introduced.
	 */
	public function createUuidV5(UuidInterface|string $namespace, string $name) : UuidInterface {
		//
		if(is_string($namespace)):
			$ns = match ( strtolower($namespace) ) {
				'dns', Uuid::NAMESPACE_DNS => Uuid::NAMESPACE_DNS,
				'url', Uuid::NAMESPACE_URL => Uuid::NAMESPACE_URL,
				'oid', Uuid::NAMESPACE_OID => Uuid::NAMESPACE_OID,
				'x500', Uuid::NAMESPACE_X500 => Uuid::NAMESPACE_X500,
				default => throw new InvalidArgumentException(sprintf('Invalid namespace %s.', $namespace))
			};
		endif;
		
		//
		return Uuid::uuid5($ns ?? $namespace, $name);
	}
}
