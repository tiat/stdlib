<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Time;

//
use DateException;
use DateInterval;
use DateMalformedIntervalStringException;
use DateMalformedStringException;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Tiat\Standard\Time\TimestampInterface;
use Tiat\Stdlib\Exception\DateInvalidTimeZoneException;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Tiat\Stdlib\Exception\RuntimeException;

use function date_default_timezone_get;
use function sprintf;
use function str_replace;
use function strtoupper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TimestampTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_TIME_FORMAT = 'Y-m-d H:i:s';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_TIMEZONE = 'UTC';
	
	/**
	 * @var null|DateTimeZone
	 * @since   3.0.0 First time introduced.
	 */
	private ?DateTimeZone $_timezone;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_timeFormat = DateTimeInterface::ATOM;
	
	/**
	 * @param    string         $format
	 * @param    NULL|string    $datetime
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function createFrom(string $format = self::DEFAULT_TIME_FORMAT, ?string $datetime = NULL) : string {
		return DateTimeImmutable::createFromFormat($format, $this->_fixDatetime($datetime))
		                        ->format($this->getTimeFormat());
	}
	
	/**
	 * This will fix the mm=0 and/or dd=0 issue when 1970-00-00 is converted to 1969-11-30 (when it should be 1970-01-01)
	 *
	 * @param    string    $datetime
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _fixDatetime(string $datetime) : string {
		return str_replace("-00", '-01', $datetime);
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTimeFormat() : string {
		return $this->_timeFormat;
	}
	
	/**
	 * @param    string    $format
	 *
	 * @return TimestampInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTimeFormat(string $format) : TimestampInterface {
		//
		$this->_timeFormat = $format;
		
		//
		return $this;
	}
	
	/**
	 * Returns a new instance of DateTimeImmutable based on the provided timestamp and timezone. If no timestamp is
	 * provided, it uses the internal timestamp of the class. If no timezone is provided, it uses the internal timezone of the class.
	 *
	 * @param    null|string          $timestamp    The timestamp to create the DateTimeImmutable instance with. Defaults to the internal timestamp if not provided.
	 * @param    null|DateTimeZone    $timezone     The timezone to create the DateTimeImmutable instance with. Defaults to the internal timezone if not provided.
	 *
	 * @return DateTimeImmutable A new instance of DateTimeImmutable.
	 * @throws DateInvalidTimeZoneException
	 * @since 3.0.0 First time introduced.
	 */
	public function getDateTimeImmutable(?string $timestamp = NULL, ?DateTimeZone $timezone = NULL) : DateTimeImmutable {
		try {
			return new DateTimeImmutable($timestamp ?? $this->getTimeStamp(), $timezone ?? $this->getTimezone());
		} catch(DateInvalidTimeZoneException|Exception $e) {
			throw new DateInvalidTimeZoneException($e->getMessage());
		}
	}
	
	/**
	 * Return the current timestamp. As default this will return ISO 8601 time in UTC format (example 2024-11-08T14:30:45+00:00).
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTimeStamp() : string {
		try {
			return ( new DateTimeImmutable("now", $this->getTimezone()) )->format($this->getTimeFormat());
		} catch(\DateInvalidTimeZoneException|DateMalformedStringException|Exception $e) {
			throw new RuntimeException($e->getMessage());
		}
	}
	
	/**
	 * @return null|DateTimeZone
	 * @throws DateInvalidTimeZoneException
	 * @since   3.0.0 First time introduced.
	 */
	public function getTimezone() : ?DateTimeZone {
		if(empty($this->_timezone)):
			try {
				$this->setTimezone(new DateTimeZone(date_default_timezone_get() ?? self::DEFAULT_TIMEZONE));
			} catch(DateInvalidTimeZoneException|DateException|Exception $e) {
				throw new DateInvalidTimeZoneException($e->getMessage());
			}
		endif;
		
		//
		return $this->_timezone;
	}
	
	/**
	 * @param    DateTimeZone    $timeZone
	 *
	 * @return TimestampInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTimezone(DateTimeZone $timeZone) : TimestampInterface {
		//
		$this->_timezone = $timeZone;
		
		//
		return $this;
	}
	
	/**
	 * Sets the date interval.
	 *
	 * @param    DateInterval     $interval    The interval as object. Use createDateInterval() to create an interval.
	 * @param    null|DateTime    $datetime    The datetime object (optional).
	 *
	 * @throws Exception
	 * @since   3.0.0 First time introduced.
	 */
	public function setDateInterval(DateInterval $interval, ?DateTime $datetime = NULL) : DateTime {
		//
		if($datetime === NULL):
			$datetime = new DateTime($this->getTimeStamp());
		endif;
		
		//
		return $datetime->add($interval);
	}
	
	/**
	 * Creates a DateInterval object based on the given interval and unit.
	 *
	 * @param    int       $interval    The value of the interval.
	 * @param    string    $unit        The unit of the interval. Possible values: 'D' for days, 'W' for weeks, 'M' for months, 'Y' for years, 'S' for seconds.
	 *
	 * @return   DateInterval|null    The created DateInterval object or null if invalid unit is provided.
	 * @since   3.0.0 First time introduced.
	 */
	public function createDateInterval(int $interval, string $unit) : ?DateInterval {
		// Create the DateInterval object based on the provided interval and unit.
		$i = match ( strtoupper($unit) ) {
			'D' => sprintf('P%uD', $interval),
			'W' => sprintf('P%uW', $interval),
			'M' => sprintf('P%uM', $interval),
			'Y' => sprintf('P%uY', $interval),
			'S' => sprintf('PT%uS', $interval),
			default => throw new InvalidArgumentException(sprintf('Invalid unit "%s". Possible values: D, W, M, Y and S',
			                                                      $unit))
		};
		
		// Return the created DateInterval object or throw an InvalidArgumentException if the unit is invalid.
		try {
			return new DateInterval($i) ?: NULL;
		} catch(Exception|DateMalformedIntervalStringException  $e) {
			throw new InvalidArgumentException($e->getMessage());
		}
	}
}
