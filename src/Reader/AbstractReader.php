<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Reader;

//
use Jantia\Stdlib\Message\Message;
use Tiat\Standard\Reader\ReaderInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractReader extends Message implements ReaderInterface {
	
	/**
	 * Directory of the file to process.
	 *
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_directory;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDirectory() : ?string {
		return $this->_directory ?? NULL;
	}
	
	/**
	 * @param    null|string    $directory
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setDirectory(?string $directory) : static {
		//
		$this->_directory = $directory;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $data
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _parseData(array $data) : array;
}
