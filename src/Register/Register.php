<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Tiat\Standard\Register\RegisterInterface;

/**
 * Register offers system-wide register services and will contain way to access all registered Registers directly. Please
 * notice that this class is not using fully qualified class name when object will be registered (for user-friendly use case).
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Register extends AbstractRegister implements RegisterInterface, RegisterHelperInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use RegisterHelperTrait;
}
