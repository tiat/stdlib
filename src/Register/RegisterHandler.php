<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Override;
use ReflectionClass;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Tiat\Stdlib\Exception\RuntimeException;

use function array_map;
use function class_exists;
use function explode;
use function get_class;
use function implode;
use function sprintf;
use function str_contains;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class RegisterHandler extends AbstractRegisterHandler {
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_registerHandler;
	
	/**
	 * @param    null|RegisterInterface    $handler
	 * @param    null|string               $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function checkRegisterHandler(?RegisterInterface $handler = NULL, ?string $name = NULL) : bool {
		//
		if(empty($name)):
			if($handler !== NULL):
				$name = $this->_resolveRegisterHandlerName($handler);
			else:
				throw new InvalidArgumentException("You must give either 'handler' or 'name' value.");
			endif;
		else:
			$name = $this->_checkRegisterHandlerName($name);
		endif;
		
		//
		return isset($this->_registerHandler[$name]);
	}
	
	/**
	 * @param    RegisterInterface    $handler
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resolveRegisterHandlerName(RegisterInterface $handler) : string {
		return $this->_checkRegisterHandlerName(( new ReflectionClass($handler) )->getShortName());
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkRegisterHandlerName(string $name) : string {
		//
		if(str_contains($name, '\\') === TRUE):
			$map = explode('\\', $name);
		elseif(str_contains($name, '_') === TRUE):
			$map = explode('_', $name);
		endif;
		
		if(! empty($map)):
			//
			$map = array_map('ucfirst', $map);
			
			//
			$map[0] = strtolower($map[0]);
			
			//
			return implode('', $map);
		endif;
		
		//
		return $name;
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function resetRegisterHandler() : RegisterHandlerInterface {
		//
		unset($this->_registerHandler);
		
		//
		return $this;
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function setRegisterHandlerDefault(string $handler, bool $overwrite = FALSE) : RegisterHandlerInterface {
		//
		if($overwrite === TRUE || $this->getRegisterHandlerDefault() === NULL):
			if($this->checkRegisterHandlerDefault($handler) === TRUE):
				$this->_registerHandlerDefault = $handler;
			else:
				$msg = sprintf("Given handler %s is not a valid interface of %s.", $handler, RegisterInterface::class);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			throw new RuntimeException("Can't set new default register handler because it's already defined and override is not allowed.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function getRegisterHandlerDefault() : ?string {
		return $this->_registerHandlerDefault ?? NULL;
	}
	
	/**
	 * @param    string    $handler    *
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function checkRegisterHandlerDefault(string $handler) : bool {
		return class_exists($handler) && ( new $handler() ) instanceof RegisterInterface;
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function resetRegisterHandlerDefault() : RegisterHandlerInterface {
		//
		if($this->checkRegisterHandlerDefault(self::DEFAULT_REGISTER_INTERFACE) === TRUE):
			$this->_registerHandlerDefault = self::DEFAULT_REGISTER_INTERFACE;
		else:
			$msg = sprintf("Can't reset the Register handler to system default (%s). Maybe it's missing?",
			               self::DEFAULT_REGISTER_INTERFACE);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function getRegisterHandler(string $name) : ?RegisterInterface {
		return $this->_registerHandler[$this->_checkRegisterHandlerName($name)] ?? NULL;
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function setRegisterHandler(RegisterInterface $handler, ?string $name = NULL, bool $overwrite = FALSE) : RegisterHandlerInterface {
		// Get & check handler name (class internal standard)
		if(empty($name)):
			$name = $this->_resolveRegisterHandlerName($handler);
		else:
			$name = $this->_checkRegisterHandlerName($name);
		endif;
		
		//
		if(! isset($this->_registerHandler[$name]) || $overwrite === TRUE):
			$this->_registerHandler[$name] = $handler;
		else:
			$msg = sprintf("Trying to insert new value %s when register handler %s already exists.", $name,
			               get_class($this->_registerHandler[$name]));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $value
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _throwError(string $value) : void {
		$msg = sprintf("No registered (Register) handler available for %s.", $value);
		throw new RuntimeException($msg);
	}
	
	/**
	 * @inheritDoc
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] protected function _checkRegisterHandlerDefault() : bool {
		//
		if($this->checkRegisterHandlerDefault(self::DEFAULT_REGISTER_INTERFACE) === FALSE):
			$msg = sprintf("Default Register handler %s is not installed.", self::DEFAULT_REGISTER_INTERFACE);
			throw new RuntimeException($msg);
		endif;
		
		//
		return TRUE;
	}
}
