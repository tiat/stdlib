<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Tiat\Standard\Register\RegisterPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RegisterHelperInterface {
	
	/**
	 * Find the key from given Registers or from ALL registers.
	 *
	 * @param    string|int    $key
	 * @param    array         $src
	 * @param    null|mixed    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function findKey(string|int $key, array $src = [], mixed $default = NULL) : mixed;
	
	/**
	 * Find the given key from given register
	 *
	 * @param    string|int                 $key
	 * @param    RegisterPluginInterface    $register
	 * @param    mixed|NULL                 $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function findRegisterValueByKey(string|int $key, RegisterPluginInterface $register, mixed $default = NULL) : mixed;
}
