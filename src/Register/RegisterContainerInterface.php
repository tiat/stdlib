<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RegisterContainerInterface extends ParametersPluginInterface {
	
	/**
	 * Get the name of the register container.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string;
	
	/**
	 * Set the name of the register container.
	 *
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : static;
	
	/**
	 * Adds a container to the registration list.
	 *
	 * @param    RegisterContainerInterface    $container    The container to add.
	 * @param    string                        $name         The name of the container.
	 *
	 * @return RegisterContainerInterface This method returns an instance of the class implementing it.
	 * @since   3.0.0 First time introduced.
	 */
	public function addContainer(RegisterContainerInterface $container, string $name) : RegisterContainerInterface;
	
	/**
	 * Get a container by name.
	 *
	 * @param    string    $name    The name of the container.
	 *
	 * @return RegisterContainerInterface|null The container with the given name or null if not found.
	 * @since   3.0.0 First time introduced.
	 */
	public function getContainer(string $name) : ?RegisterContainerInterface;
	
	/**
	 * Delete a container by name.
	 *
	 * @param    string    $name    The name of the container to delete.
	 *
	 * @return RegisterContainerInterface The current instance of the class.
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteContainer(string $name) : RegisterContainerInterface;
	
	/**
	 * Determine if a container exists by name.
	 *
	 * @param    string    $name    The name of the container.
	 *
	 * @return bool True if a container with the given name exists, false otherwise.
	 * @since   3.0.0 First time introduced.
	 */
	public function hasContainer(string $name) : bool;
	
	/**
	 * Get all containers.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getContainers() : ?array;
	
	/**
	 * Search for a value in the given header and key.
	 *
	 * @param    string               $header       The header to search for the value.
	 * @param    RegisterContainer    $container    The container to search in (optional).
	 * @param    string               $key          The key to search for the value.
	 *
	 * @return mixed The value found in the given header and key, or null if not found.
	 * @since   3.0.0 First time introduced.
	 */
	public function searchValue(string $header, RegisterContainer $container, string $key) : mixed;
}
