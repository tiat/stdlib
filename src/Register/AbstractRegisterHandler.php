<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Jantia\Stdlib\Message\Message;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Stdlib\Loader\ClassLoaderTrait;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function get_class;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractRegisterHandler extends Message implements ClassLoaderInterface, ParametersPluginInterface, RegisterHandlerInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_REGISTER_INTERFACE = Register::class;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_registerHandlerDefault = self::DEFAULT_REGISTER_INTERFACE;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		//
		parent::__construct();
		
		//
		$this->_setConstruct($params);
		
		//
		$this->setAutoInit($autoinit)->setAutoRun($autorun);
	}
	
	/**
	 * @param    iterable|NULL    $params
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _setConstruct(iterable $params = []) : void {
		//
		$this->setParams($params);
		
		//
		if($this->_checkRegisterHandlerDefault() === TRUE):
			//
			$class = get_class($this);
			$name  = $this->getRegisterHandlerDefault();
			if($this->checkRegisterHandler(name:$class) === FALSE):
				$this->setRegisterHandler(new $name(), $class);
			endif;
		endif;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _checkRegisterHandlerDefault() : bool;
	
	/**
	 * @param    iterable|NULL    $params
	 * @param                     ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function __invoke(iterable $params = [], ...$args) : ClassLoaderInterface {
		//
		$this->_setConstruct($params);
		
		//
		return $this->init(...$args)->run(...$args);
	}
	
	/**
	 * @param    RegisterInterface    $handler
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _resolveRegisterHandlerName(RegisterInterface $handler) : string;
	
	/**
	 * @param    string    $name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _checkRegisterHandlerName(string $name) : string;
	
	/**
	 * @param    string    $value
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _throwError(string $value) : void;
}
