<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use ArrayAccess;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class RegisterContainer implements RegisterContainerInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin {
		ParametersPlugin::getParam as getParamFromPlugin;
		ParametersPlugin::hasParam as hasParamFromPlugin;
		ParametersPlugin::setParam as setParamFromPlugin;
		ParametersPlugin::setParams as setParamsFromPlugin;
		ParametersPlugin::resetParam as resetParamFromPlugin;
	}
	
	/**
	 * @var string $_name The name of the register container.
	 * @since   3.0.0 First time introduced.
	 */
	private string $_name;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_containers;
	
	/**
	 * @param    string    $name
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string $name) {
		$this->setName($name);
	}
	
	/**
	 * @param    iterable|ArrayAccess    $params
	 * @param    bool                    $override
	 * @param    bool                    $paramValidation
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setParams(iterable|ArrayAccess $params, bool $override = FALSE, bool $paramValidation = TRUE) : static {
		//
		$this->setParamsFromPlugin($params, $override, $paramValidation);
		
		//
		return $this;
	}
	
	/**
	 * Adds a container to the collection.
	 *
	 * @param    RegisterContainerInterface    $container    The container to add.
	 * @param    null|string                   $name         The name of the container.
	 *
	 * @return static The updated instance of the class.
	 * @since   3.0.0 First time introduced.
	 */
	public function addContainer(RegisterContainerInterface $container, ?string $name = NULL) : static {
		//
		$this->setParam($name ?? $container->getName(), $container);
		$this->_containers[$name ?? $container->getName()] = TRUE;
		
		//
		return $this;
	}
	
	/**
	 * Set parameter in the container.
	 *
	 * @param    int|string    $key
	 * @param    mixed|NULL    $value
	 * @param    bool          $override
	 * @param    bool          $paramValidation
	 * @param    bool          $throwError
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setParam(int|string $key, mixed $value = NULL, bool $override = FALSE, bool $paramValidation = TRUE, bool $throwError = TRUE) : static {
		//
		$this->setParamFromPlugin(strtolower((string)$key), $value, $override, $paramValidation, $throwError);
		
		//
		return $this;
	}
	
	/**
	 * Get the name of the register container.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string {
		//
		return $this->_name;
	}
	
	/**
	 * Sets the name of the register container.
	 *
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//
		$this->_name = strtolower($name);
		
		//
		return $this;
	}
	
	/**
	 * Deletes a specific container by name.
	 *
	 * @param    string    $name    The name of the container to delete.
	 *
	 * @return static Returns the current instance of the class after the deletion.
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteContainer(string $name) : static {
		//
		if($this->hasParam($name) === TRUE && $this->getParam($name) instanceof RegisterContainerInterface):
			$this->resetParam($name);
			unset($this->_containers[$name]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    int|string    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasParam(int|string $key) : bool {
		return $this->hasParamFromPlugin(strtolower((string)$key));
	}
	
	/**
	 * Get parameter from the container.
	 *
	 * @param    NULL|int|string    $key
	 * @param    mixed|NULL         $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getParam(int|string|null $key = NULL, mixed $default = NULL) : mixed {
		return $this->getParamFromPlugin(strtolower((string)$key), $default);
	}
	
	/**
	 * @param    int|string    $key
	 *
	 * @return RegisterContainer
	 * @since   3.0.0 First time introduced.
	 */
	public function resetParam(int|string $key) : static {
		//
		$this->resetParamFromPlugin(strtolower((string)$key));
		
		//
		return $this;
	}
	
	/**
	 * Search for a value in the parameter list including all subcontainers.
	 *
	 * @param    string                             $header    The header value to search for (Container name).
	 * @param    null|RegisterContainerInterface    $container
	 * @param    null|string                        $key       The key value to search for.
	 *
	 * @return mixed|null The found value or null if not found.
	 * @since   3.0.0 First time introduced.
	 */
	public function searchValue(string $header, ?RegisterContainerInterface $container = NULL, ?string $key = NULL) : mixed {
		//
		$header = strtolower($header);
		
		//
		$container = $container ?? $this;
		
		// Get keys from all param
		if($container->getName() === $header):
			return $container;
		elseif($container->hasContainer($header)):
			if($key !== NULL):
				return ( $container->getContainer($header) )->getParam($key);
			else:
				return ( $container->getContainer($header) );
			endif;
		else:
			// Go through all subcontainers
			$keys = array_keys($this->getContainers());
			foreach($keys as $k):
				if($k instanceof RegisterContainerInterface):
					if($k->hasContainer($header)):
						// Value found in subcontainer
						return ( $k->getContainer($header) )->getParam($key);
					else:
						//
						return $this->searchValue($header, $k, $key);
					endif;
				endif;
			endforeach;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Checks if a specific container exists.
	 *
	 * @param    string    $name    The name of the container to check.
	 *
	 * @return bool True if the container exists, false otherwise.
	 * @since   3.0.0 First time introduced.
	 */
	public function hasContainer(string $name) : bool {
		//
		return isset($this->_containers[$name]);
	}
	
	/**
	 * Retrieves a specific container by name.
	 *
	 * @param    string    $name    The name of the container to retrieve.
	 *
	 * @return RegisterContainerInterface|null The requested container or null if it does not exist.
	 * @since   3.0.0 First time introduced.
	 */
	public function getContainer(string $name) : ?RegisterContainerInterface {
		//
		if(isset($this->_containers[$name])):
			return $this->getParam($name);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getContainers() : ?array {
		return $this->_containers ?? NULL;
	}
}
