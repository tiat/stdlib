<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Tiat\Standard\Register\RegisterInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RegisterHandlerInterface {
	
	/**
	 * Return TRUE if register handler is defined.
	 *
	 * @param    RegisterInterface    $handler
	 * @param    string               $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkRegisterHandler(RegisterInterface $handler, string $name) : bool;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterHandler(string $name) : ?RegisterInterface;
	
	/**
	 * @param    RegisterInterface    $handler
	 * @param    null|string          $name
	 * @param    bool                 $overwrite
	 *
	 * @return RegisterHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterHandler(RegisterInterface $handler, ?string $name = NULL, bool $overwrite = FALSE) : RegisterHandlerInterface;
	
	/**
	 * @return RegisterHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRegisterHandler() : RegisterHandlerInterface;
	
	/**
	 * Check is the register handler valid or not.
	 *
	 * @param    string    $handler
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkRegisterHandlerDefault(string $handler) : bool;
	
	/**
	 * Get default register handler.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterHandlerDefault() : ?string;
	
	/**
	 * Set default register handler.
	 *
	 * @param    string    $handler
	 * @param    bool      $overwrite
	 *
	 * @return RegisterHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterHandlerDefault(string $handler, bool $overwrite = FALSE) : RegisterHandlerInterface;
	
	/**
	 * Reset default register handler to system default.
	 *
	 * @return RegisterHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRegisterHandlerDefault() : RegisterHandlerInterface;
}
