<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

use Tiat\Standard\Register\RegisterPluginInterface;

/**
 * Trait RegisterHelperTrait
 * Trait containing helper methods for registering and retrieving values.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait RegisterHelperTrait {
	
	/**
	 * @param    string|int    $key
	 * @param    array         $src
	 * @param    null|mixed    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function findKey(string|int $key, array $src = [], mixed $default = NULL) : mixed {
		// Limit search to given registers
		if(! empty($src)):
			foreach($src as $reg):
				if(( $val = $this->findRegisterValueByKey($key, self::pullRegister(new $reg())) ) !== NULL):
					return $val;
				endif;
			endforeach;
		else:
			// Get SplObjectStorage, reset & loop until value has been found
			$e = self::$_globalRegister;
			$e->rewind();
			while($e->valid()):
				if(( $val = $this->findRegisterValueByKey($key, $e->current()) ) !== NULL):
					return $val;
				endif;
				
				//
				$e->next();
			endwhile;
		endif;
		
		//
		return $default;
	}
	
	/**
	 * @param    string|int                 $key
	 * @param    RegisterPluginInterface    $register
	 * @param    mixed|NULL                 $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function findRegisterValueByKey(string|int $key, RegisterPluginInterface $register, mixed $default = NULL) : mixed {
		return $register->getRegisterKey($key) ?? $default;
	}
}
