<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use Jantia\Stdlib\Message\Message;
use ReflectionClass;
use ReflectionException;
use SplObjectStorage;
use Tiat\Config\Loader\ConfigLoader;
use Tiat\Standard\Config\ConfigLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Tiat\Stdlib\Exception\RuntimeException;

use function get_debug_type;
use function is_object;
use function is_scalar;
use function is_string;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractRegister extends Message implements ConfigLoaderInterface, ParametersPluginInterface, RegisterInterface, RegisterPluginInterface {
	
	//
	use ConfigLoader;
	use RegisterPluginTrait;
	
	/**
	 * @var SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	protected static SplObjectStorage $_globalRegister;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_containers;
	
	/**
	 * @param    string|(RegisterPluginInterface&ParametersPluginInterface)    $register
	 * @param    NULL|string                                                   $passphrase
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public static function deleteRegister(( RegisterPluginInterface&ParametersPluginInterface )|string $register, ?string $passphrase = NULL) : void {
		if(self::checkRegister($register) === TRUE):
			( static::_getRegisterInterface() )->offsetUnset(self::getRegister($register, $passphrase));
		endif;
	}
	
	/**
	 * @param    string|(RegisterPluginInterface&ParametersPluginInterface)    $register
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public static function checkRegister(( RegisterPluginInterface&ParametersPluginInterface )|string $register) : bool {
		//
		if(is_string($register) && self::_getRegisterByName($register) !== NULL):
			return TRUE;
		elseif(is_object($register)):
			return ( static::_getRegisterInterface() )->offsetExists($register);
		endif;
		
		// Otherwise return false (register don't exist at all)
		return FALSE;
	}
	
	/**
	 * Try fetch the register if it's already declared. Will return NULL if not found and throw exception if passphrase doesn't match.
	 *
	 * @param    string         $name
	 * @param    null|string    $passphrase
	 *
	 * @return null|RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	private static function _getRegisterByName(string $name, ?string $passphrase = NULL) : ( RegisterPluginInterface&ParametersPluginInterface )|null {
		// Go to first element
		( $r = static::_getRegisterInterface() )->rewind();
		
		// Loop all values until...
		$name = strtolower($name);
		while($r->valid()):
			// Try find to object with same name
			if(! empty($info = $r->getInfo()) && $name === strtolower($info['name'] ?? NULL)):
				if(empty($info['passphrase']) || $info['passphrase'] === $passphrase):
					return $r->current();
				else:
					// Register is correct but passphrase doesn't match
					throw new RuntimeException("Can't fetch the register because passphrase doesn't match.");
				endif;
			endif;
			
			// Else go to next
			$r->next();
		endwhile;
		
		//
		return NULL;
	}
	
	/**
	 * Get Registry tree or create new if not exists.
	 *
	 * @return SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	protected static function _getRegisterInterface() : SplObjectStorage {
		//
		if(empty(self::$_globalRegister)):
			self::$_globalRegister = new SplObjectStorage();
		endif;
		
		//
		return self::$_globalRegister;
	}
	
	/**
	 * @param    string|(RegisterPluginInterface&ParametersPluginInterface)    $register
	 * @param    null|string                                                   $passphrase
	 *
	 * @return null|(RegisterPluginInterface&ParametersPluginInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public static function getRegister(( RegisterPluginInterface&ParametersPluginInterface )|string $register, ?string $passphrase = NULL) : ( RegisterPluginInterface&ParametersPluginInterface )|null {
		// Try convert to object with Register Tree
		if(is_string($register)):
			return self::_getRegisterByName($register, $passphrase);
		elseif($register instanceof RegisterPluginInterface &&
		       ( $r = static::_getRegisterInterface() )->offsetExists($register)):
			// Check the Register interface passphrase
			$result = $r->offsetGet($register);
			if($passphrase === NULL || $result['passphrase'] === $passphrase):
				return $result;
			else:
				throw new RuntimeException("Can't fetch the register because passphrase doesn't match.");
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    null|string                                          $passphrase
	 *
	 * @return RegisterPluginInterface&ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public static function pullRegister(RegisterPluginInterface&ParametersPluginInterface $register, ?string $passphrase = NULL) : RegisterPluginInterface&ParametersPluginInterface {
		//
		if(( self::checkRegister(( $name = ( new ReflectionClass($register) )->getShortName() )) ) === TRUE):
			// Use old instance because Register already exists
			return self::getRegister($name);
		else:
			// Register don't exist so define new instance
			self::setRegister($register, passphrase:$passphrase);
			
			//
			return $register;
		endif;
	}
	
	/**
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    mixed                                                $value
	 * @param    bool                                                 $overwrite
	 * @param    null|string                                          $passphrase
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public static function setRegister(RegisterPluginInterface&ParametersPluginInterface $register, mixed $value = NULL, bool $overwrite = FALSE, ?string $passphrase = NULL) : void {
		// Fetch value from Register instance
		if($value === NULL || is_object($value)):
			try {
				$value = ( new ReflectionClass($value ?? $register) )->getShortName();
			} catch(ReflectionException $e) {
				throw new InvalidArgumentException($e->getMessage());
			}
		elseif(is_scalar($value) === TRUE):
			// Casting value to string
			$value = (string)$value;
		else:
			$msg = sprintf("Value have to be scalar or object. Got the %s.", get_debug_type($value));
			throw new InvalidArgumentException($msg);
		endif;
		
		// Register the Register, and it's name as string
		// Notice! It's important to check Register by its name (string) than with object
		if($overwrite === TRUE || self::checkRegister($value) === FALSE):
			self::_setRegister($register, $value, $passphrase);
		else:
			$msg = sprintf("Register already contains '%s' and overwrite is not allowed.", $value);
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    mixed                                                $value
	 * @param    NULL|string                                          $passphrase
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private static function _setRegister(RegisterPluginInterface&ParametersPluginInterface $register, mixed $value, ?string $passphrase = NULL) : void {
		( static::_getRegisterInterface() )->offsetSet($register, ['name' => $value, 'passphrase' => $passphrase]);
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterContainers() : ?array {
		return $this->_containers ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|RegisterContainerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterContainer(string $name) : ?RegisterContainerInterface {
		return $this->_containers[$name] ?? NULL;
	}
	
	/**
	 * Sets the register container.
	 *
	 * @param    RegisterContainerInterface    $container    The register container to be set.
	 *
	 * @return static The updated instance of the
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterContainer(RegisterContainerInterface $container) : static {
		//
		$this->_containers[$container->getName()] = $container;
		
		//
		return $this;
	}
}
