<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Stdlib\Register;

//
use BackedEnum;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function get_debug_type;
use function is_int;
use function is_string;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait RegisterPluginTrait {
	
	//
	use ParametersPlugin;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_registerTree;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterKeyAll() : ?array {
		return $this->_registerTree ?? NULL;
	}
	
	/**
	 * @param    string|int|object    $key
	 * @param    null|mixed           $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterKey(string|int|object $key, mixed $default = NULL) : mixed {
		//
		if(( $index = $this->_getRegisterKey($key, TRUE) ) !== NULL):
			return $this->_registerTree[$index] ?? $default;
		endif;
		
		//
		return $default;
	}
	
	/**
	 * @param    mixed    $key
	 * @param    bool     $error
	 *
	 * @return null|string|int
	 * @since   3.0.0 First time introduced.
	 */
	private function _getRegisterKey(mixed $key, bool $error = FALSE) : null|string|int {
		if($this->_checkRegisterKey($key) === TRUE):
			if(is_int($key) || is_string($key)):
				return $key;
			elseif($key instanceof BackedEnum):
				return $key->value;
			elseif($error === TRUE):
				// Throw error if requested
				$msg = sprintf("Key must be string, int or backed enum. Got the type %s.", get_debug_type($key));
				throw new InvalidArgumentException($msg);
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * This is extra check method for the config key and can be customized from application.
	 * Overwrite this method if you want to restrict or check that your config key to be something special like enum value.
	 * Just notice that key MUST be string or int (or enum) finally.
	 *
	 * @param    mixed    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkRegisterKey(mixed $key) : bool {
		return TRUE;
	}
	
	/**
	 * @param    array    $config
	 * @param    bool     $overwrite
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterKeyAll(array $config, bool $overwrite = FALSE) : RegisterPluginInterface {
		//
		if(! empty($config)):
			foreach($config as $key => $val):
				$this->setRegisterKey($key, $val, $overwrite);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int|object    $key
	 * @param    mixed                $value
	 * @param    bool                 $overwrite
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterKey(string|int|object $key, mixed $value, bool $overwrite = FALSE) : RegisterPluginInterface {
		//
		if(( $index = $this->_getRegisterKey($key, TRUE) ) !== NULL):
			//
			if(! isset($this->_registerTree[$index]) || $overwrite === TRUE):
				$this->_registerTree[$index] = $value;
			else:
				$msg = sprintf("Value '%s' already exists in register and overwrite is denied.", $index);
				throw new InvalidArgumentException($msg);
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int|object    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkRegisterKey(string|int|object $key) : bool {
		//
		if(( $index = $this->_getRegisterKey($key, TRUE) ) !== NULL):
			return isset($this->_registerTree[$index]);
		endif;
		
		// Return false as default because index key is not casting
		return FALSE;
	}
	
	/**
	 * @param    string|int|object    $key
	 * @param    mixed|NULL           $default
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRegisterKey(string|int|object $key, mixed $default = NULL) : RegisterPluginInterface {
		//
		if(( $index = $this->_getRegisterKey($key, TRUE) ) !== NULL):
			$this->_registerTree[$index] = $default;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int|object    $key
	 *
	 * @return RegisterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteRegisterKey(string|int|object $key) : RegisterPluginInterface {
		//
		if(( $index = $this->_getRegisterKey($key, TRUE) ) !== NULL):
			unset($this->_registerTree[$index]);
		endif;
		
		//
		return $this;
	}
}
