<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Parameters;

//
use ArrayObject;
use Tiat\Standard\Parameters\ParametersInterface;

use function http_build_query;
use function parse_str;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Parameters extends ArrayObject implements ParametersInterface {
	
	/**
	 * Enforces that we have an array, and enforces parameter access to array elements.
	 *
	 * @param    array    $values
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(array $values) {
		parent::__construct($values, flags:ArrayObject::ARRAY_AS_PROPS);
	}
	
	/**
	 * @param    string    $string
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function fromString(string $string) : void {
		//
		$array = [];
		
		//
		parse_str($string, result:$array);
		$this->fromArray(values:$array);
	}
	
	/**
	 * @param    array    $values
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function fromArray(array $values) : void {
		$this->exchangeArray($values);
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function toString() : string {
		return http_build_query(data:$this->toArray());
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : array {
		return $this->getArrayCopy();
	}
	
	/**
	 * @param    string        $name
	 * @param    null|mixed    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function get(string $name, mixed $default = NULL) : mixed {
		//
		if($this->offsetExists(key:$name)):
			return parent::offsetGet(key:$name);
		endif;
		
		//
		return $default;
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetGet(mixed $key) : mixed {
		//
		if($this->offsetExists(key:$key)):
			return parent::offsetGet(key:$key);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return ParametersInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function set(string $name, mixed $value) : ParametersInterface {
		//
		$this[$name] = $value;
		
		//
		return $this;
	}
}
