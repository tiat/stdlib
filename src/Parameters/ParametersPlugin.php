<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Stdlib\Parameters;

//
use ArrayAccess;
use Laminas\Stdlib\ArrayUtils;
use ReflectionClass;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Traversable;

use function get_debug_type;
use function implode;
use function is_array;
use function is_object;
use function is_resource;
use function is_scalar;
use function is_string;
use function mb_check_encoding;
use function mb_list_encodings;
use function mb_strtolower;
use function sprintf;
use function strtolower;
use function trim;

/**
 * @version 3.2.1
 * @since   3.0.0 First time introduced.
 */
trait ParametersPlugin {
	
	/**
	 * Set default encoding
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string ENCODING_DEFAULT = 'UTF-8';
	
	/**
	 * Param case insensitive (default: ON)
	 *
	 * @since   3.2.1 First time introduced.
	 */
	public const bool DEFAULT_CASE_INSENSITIVE = TRUE;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_params;
	
	/**
	 * @var ?string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_encoding = self::ENCODING_DEFAULT ?? 'UTF-8';
	
	/**
	 * Param validation (default: ON)
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_validateParam = TRUE;
	
	/**
	 * @var bool
	 * @since   3.2.1 First time introduced.
	 */
	private bool $_caseInsensitive = self::DEFAULT_CASE_INSENSITIVE;
	
	/**
	 * Prevent overloading
	 *
	 * @param    string    $name
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	final public function __get(string $name) : mixed {
		return $this->getParam($name);
	}
	
	/**
	 * Prevent overloading
	 *
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	final public function __set(string $name, mixed $value) : void {
		$this->setParam($name, $value);
	}
	
	/**
	 * @param    NULL|string|int    $key
	 * @param    mixed|NULL         $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 * @since   3.2.1 Added param key validation.
	 */
	public function getParam(string|int|null $key = NULL, mixed $default = NULL) : mixed {
		if(! empty($key = $this->_checkParamKey($key))):
			if($this->getCaseInsensitive() === TRUE):
				return $this->_params[$key] ?? $default;
			else:
				// We must parse all params through
				foreach($this->_params as $k => $v):
					if(mb_strtolower((string)$k, $this->getEncoding()) === mb_strtolower($key, $this->getEncoding())):
						return $v;
					endif;
				endforeach;
			endif;
		endif;
		
		//
		return $default;
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return string
	 * @since   3.2.1 First time introduced.
	 */
	private function _checkParamKey(mixed $key) : string {
		if(! is_scalar($key)):
			throw new InvalidArgumentException("Invalid key type. Only scalar values are allowed.");
		endif;
		
		// Trim and convert all letters to UTF8
		return trim(mb_convert_encoding((string)$key, self::ENCODING_DEFAULT, 'auto'));
	}
	
	/**
	 * @return bool
	 * @since   3.2.1 First time introduced.
	 */
	public function getCaseInsensitive() : bool {
		return $this->_caseInsensitive;
	}
	
	/**
	 * @param    bool    $caseInsensitive
	 *
	 * @return $this
	 * @since   3.2.1 First time introduced.
	 */
	public function setCaseInsensitive(bool $caseInsensitive) : static {
		//
		$this->_caseInsensitive = $caseInsensitive;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : ?string {
		return $this->_encoding ?? NULL;
	}
	
	/**
	 * @param    string|int    $key
	 * @param    null|mixed    $value
	 * @param    bool          $override
	 * @param    bool          $paramValidation
	 * @param    bool          $throwError
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.2.1 Add param validation.
	 */
	public function setParam(string|int $key, mixed $value = NULL, bool $override = FALSE, bool $paramValidation = TRUE, bool $throwError = TRUE) : ParametersPluginInterface {
		// Check that key is not empty and trim
		if(( ! empty($key = $this->_checkParamKey($key)) && $override === TRUE ) || $this->hasParam($key) === FALSE):
			//
			if(( is_string($value) || is_array($value) ) &&
			   ( $this->getParamEncodingValidation() === TRUE && $paramValidation === TRUE )):
				// Check encoding
				if($this->checkEncoding($value, $this->getEncoding()) === TRUE):
					$this->_setParam($key, $value);
				endif;
			else:
				$this->_setParam($key, $value);
			endif;
		elseif($throwError === TRUE):
			$msg = sprintf("Value with key '%s' already exists in %s. Please use override=true or delete param first.",
			               $key, ( new ReflectionClass($this) )->getParentClass()->getName());
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int    $key    $
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 * @since   3.2.1 Check the key before checking if it exists.
	 */
	public function hasParam(string|int $key) : bool {
		// Don't use empty key
		if(! empty($key = $this->_checkParamKey($key))):
			return isset($this->_params[$key]);
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getParamEncodingValidation() : bool {
		return $this->_validateParam;
	}
	
	/**
	 * @param    array|string    $value
	 * @param    NULL|string     $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkEncoding(array|string $value, ?string $encoding = NULL) : bool {
		//
		if($encoding === NULL):
			$encoding = $this->getEncoding();
		elseif($this->_checkEncoding($encoding) === FALSE):
			$msg = sprintf("Given encoding (%s) is not supported.", $encoding);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		if($this->_checkEncodingValues($value, $encoding) === FALSE):
			$msg = sprintf("Given value (%s) is not valid for the specified encoding (%s).", $value, $encoding);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * Check that encoding is supported in this system
	 *
	 * @param    string    $encoding
	 *
	 * @return string|false
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkEncoding(string $encoding) : string|false {
		// Get system supported encodings
		$list = mb_list_encodings();
		
		//
		foreach($list as $val):
			if(strtolower($encoding) === strtolower($val)):
				return $val;
			endif;
		endforeach;
		
		//
		return FALSE;
	}
	
	/**
	 * Check all array values in the given array. Values must be valid for the given encoding and keys&vals must be string|array.
	 *
	 * @param    array|string    $value
	 * @param    string          $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkEncodingValues(array|string $value, string $encoding) : bool {
		//
		if(is_string($value)):
			return mb_check_encoding($value, $encoding);
		elseif(is_array($value)):
			// Check keys
			foreach($value as $key => $val):
				//
				if($this->_checkEncodingValues((string)$key, $encoding) === FALSE):
					return FALSE;
				endif;
				
				// Skip objects and resources. Those will not be checked.
				if(is_object($val) === TRUE):
					if($this->_checkEncodingFromObjects($val, $encoding) === FALSE):
						return FALSE;
					endif;
				elseif(is_resource($val) === TRUE):
					continue;
				elseif(is_array($val) === TRUE):
					return $this->_checkEncodingValues($val, $encoding);
				elseif(is_string($val = (string)$val)):
					if($this->_checkEncodingValues($val, $encoding) === FALSE):
						return FALSE;
					endif;
				else:
					throw new InvalidArgumentException(sprintf("Given value (%s) is not valid for the specified encoding (%s). Maybe type error with type %s.",
					                                           $val, $encoding, get_debug_type($val)));
				endif;
			endforeach;
			
			//
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * Check object vars with given encoding.
	 *
	 * @param    object    $value
	 * @param    string    $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkEncodingFromObjects(object $value, string $encoding) : bool {
		//
		if(! empty($vars = get_object_vars($value))):
			return $this->_checkEncodingValues($vars, $encoding);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @param    string|int    $key
	 * @param    mixed         $value
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _setParam(string|int $key, mixed $value) : void {
		$this->_params[$key] = $value;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : ParametersPluginInterface {
		if(( $value = $this->_checkEncoding($encoding) ) !== FALSE):
			$this->_encoding = $value;
		else:
			$msg = sprintf("Encoding '%s' is not supported. It must be one of follows: %s", $encoding,
			               implode(', ', mb_list_encodings()));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Prevent overloading
	 *
	 * @param    string    $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	final public function __isset(string $name) : bool {
		return $this->hasParam($name);
	}
	
	/**
	 * Prevent overloading
	 *
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	final public function __unset(string $name) : void {
		$this->deleteParam($name);
	}
	
	/**
	 * @param    string|int    $key
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.2.1 Added param key validation.
	 */
	public function deleteParam(string|int $key) : ParametersPluginInterface {
		//
		if(! empty($key = $this->_checkParamKey($key))):
			unset($this->_params[$key]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getParams() : ?array {
		return $this->_params ?? NULL;
	}
	
	/**
	 * @param    ArrayAccess|iterable    $params
	 * @param    bool                    $override
	 * @param    bool                    $paramValidation
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParams(ArrayAccess|iterable $params, bool $override = FALSE, bool $paramValidation = TRUE) : ParametersPluginInterface {
		//
		if($params instanceof Traversable):
			$params = ArrayUtils::iteratorToArray((array)$params);
		endif;
		
		//
		if(! empty($params)):
			foreach($params as $key => $val):
				$this->setParam($key, $val, $override, $paramValidation);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int    $key
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.2.1 Added param key validation.
	 */
	public function resetParam(string|int $key) : ParametersPluginInterface {
		if(! empty($key = $this->_checkParamKey($key))):
			return $this->setParam($key, NULL, TRUE);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setParamEncodingValidation(bool $status) : ParametersPluginInterface {
		//
		$this->_validateParam = $status;
		
		//
		return $this;
	}
	
	/**
	 * @return ParametersPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEncoding() : ParametersPluginInterface {
		//
		$this->_encoding = self::ENCODING_DEFAULT ?? 'UTF-8';
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    $offset
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetGet(mixed $offset) : mixed {
		return $this->getParam($offset, NULL);
	}
	
	/**
	 * @param    mixed    $offset
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetExists(mixed $offset) : bool {
		return $this->hasParam($offset);
	}
	
	/**
	 * @param    mixed    $offset
	 * @param    mixed    $value
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetSet(mixed $offset, mixed $value) : void {
		$this->setParam($offset, $value);
	}
	
	/**
	 * @param    mixed    $offset
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function offsetUnset(mixed $offset) : void {
		$this->deleteParam($offset);
	}
}
